package gadfil.apimedia.app.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    private String url;
    private int width;
    private int height;

    public Photo() {
    }

    public Photo(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }



    @Override
    public String toString() {
        return  url +String.format("\n%d, %d ", width,height);
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    private Photo(Parcel in) {
        this.url = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
