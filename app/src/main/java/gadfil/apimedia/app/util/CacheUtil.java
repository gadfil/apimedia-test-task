package gadfil.apimedia.app.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import gadfil.apimedia.app.model.Photo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by gadfil on 14.03.2015.
 */
public class CacheUtil {
    public static final String IMAGE_NAME = "PlaceCage.jpg";
    public static final String CACHE_DIR = "CACHE_DIR";

    public static void clearCache(Context context) {
        final File cache = context.getApplicationContext().getCacheDir();
        if (cache.exists()) {
            deleteFolder(cache);
        }

    }

    private static void deleteFolder(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                deleteFolder(child);
        }
        fileOrDirectory.delete();
        fileOrDirectory.delete();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static Photo[] getCacheUrls(Context context) {
        ArrayList<File> files = new ArrayList<File>();
        getFileList(new File(context.getApplicationContext().getCacheDir(), CACHE_DIR).getPath(), files);
        Photo[] photos = new Photo[files.size()];

        for (int i = 0; i < files.size(); i++) {
            Photo photo = new Photo();
            photo.setUrl(files.get(i).getPath());
            photos[i] = photo;
        }
        return photos;
    }


    static void getFileList(String directoryName, ArrayList<File> files) {

        File directory = new File(directoryName);

        if (directory.exists()) {
            // get all the files from a directory
            File[] fList = directory.listFiles();
            for (File file : fList) {
                if (file.isFile()) {
                    files.add(file);
                } else if (file.isDirectory()) {
                    getFileList(file.getAbsolutePath(), files);
                }
            }
        }
    }

    public static class CacheTask extends AsyncTask<Photo[], Void, Void> {

        Context context;

        public CacheTask(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Photo[]... params) {
            Photo[] photos = params[0];
            clearCache(context);

            for (Photo photo : photos) {
                if (isCancelled()) {
                    return null;
                }
                Bitmap bitmap = getBitmapFromURL(photo.getUrl());
                if (isCancelled()) {
                    return null;
                }
                FileOutputStream out = null;
                try {
                    File dir = new File(context.getApplicationContext().getCacheDir(), CACHE_DIR);
                    dir = new File(dir, photo.getUrl());
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    File img = new File(dir, IMAGE_NAME);
                    out = new FileOutputStream(img);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);

                    bitmap.recycle();
                    DebugLog.d("cache", "img " + img.getPath());
                    out.flush();
                    out.close();

                    if (isCancelled()) {

                        return null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
            return null;
        }


        /**
         * Download image by link
         *
         * @param link
         * @return downloaded image
         */
        public static Bitmap getBitmapFromURL(String link) {
            try {
                URL url = new URL(link);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                return myBitmap;

            } catch (IOException e) {
                e.printStackTrace();
                Log.e("getBmpFromUrl error: ", e.getMessage().toString());
                return null;
            }
        }
    }
}
