package gadfil.apimedia.app.util;

import java.util.Random;

import gadfil.apimedia.app.model.Photo;

/**
 * Created by gadfil on 14.03.2015.
 */
public class UrlUtil {

    public static final String CALM = "http://www.placecage.com/%d/%d";
    public static final String GRAY = "http://www.placecage.com/g/%d/%d";
    public static final String CRAAZY = "http://www.placecage.com/c/%d/%d";
    /**
     * Minimum image size for api
     */
    public static final int MIN_SIZE = 100;
    /**
     * Maximum image size for api
     */
    public static final int MAX_SIZE = 1024;

    private static double aspectRatioArray[] = new double[]{1.5, 1.3, 1, 1.4, 3};

    public static Photo getPhoto(String url, int minSize, int maxSize) {
        double aspectRatio =aspectRatioArray[ randInt(0, aspectRatioArray.length-1) ];
        double width = minSize + (int) (Math.random() * ((maxSize - minSize) + 1));
        double height = width * aspectRatio;
        if (height > maxSize) {
            height = width / aspectRatio;
        }
        if (height < minSize) {
            height = width;
        }
        return new Photo(String.format(url, (int)width, (int)height), (int)width, (int)height);
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
}
