package gadfil.apimedia.app.util;

import android.util.Log;
import gadfil.apimedia.app.BuildConfig;

/**
 * Created by gadfil on 14.03.2015.
 */
public class DebugLog {

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg);
        }
    }
}
