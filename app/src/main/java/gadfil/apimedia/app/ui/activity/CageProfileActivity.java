package gadfil.apimedia.app.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import butterknife.InjectView;
import gadfil.apimedia.app.R;
import gadfil.apimedia.app.model.Photo;
import gadfil.apimedia.app.ui.adapter.CageProfileAdapter;
import gadfil.apimedia.app.ui.adapter.SimplePhotoAdapter;
import gadfil.apimedia.app.ui.fragment.NavigationFragment;
import gadfil.apimedia.app.util.CacheUtil;
import gadfil.apimedia.app.util.UrlUtil;
import gadfil.apimedia.app.view.RevealBackgroundView;

public class CageProfileActivity extends BaseActivity implements NavigationFragment.NavigateAction,
        CageProfileAdapter.OptionAction, RevealBackgroundView.OnStateChangeListener  {


    String url = UrlUtil.CALM;
    CacheUtil.CacheTask cacheTask;
    Photo[] photos;
    @InjectView(R.id.rvPhotos)
    RecyclerView rvPhotos;
    @InjectView(R.id.vRevealBackground)
    RevealBackgroundView vRevealBackground;

    CageProfileAdapter adapter;

    int spanCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cage_profile);
        spanCount = 3;
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        rvPhotos.setLayoutManager(layoutManager);
        cacheTask = new CacheUtil.CacheTask(this);
        setupRevealBackground();
        rvPhotos.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                adapter.setLockedAnimations(true);
            }
        });
    }

    private void reset(boolean animate) {
        if (CacheUtil.isOnline(this)) {
            photos = new Photo[50];
            for (int i = 0; i < photos.length; i++) {
                photos[i] = UrlUtil.getPhoto(url, UrlUtil.MIN_SIZE, UrlUtil.MAX_SIZE);
            }
            if (cacheTask.getStatus() == AsyncTask.Status.RUNNING) {
                cacheTask.cancel(true);
            }
            cacheTask = new CacheUtil.CacheTask(this);
            cacheTask.execute(photos);
            adapter = new CageProfileAdapter(this, photos);
        } else {
            photos = CacheUtil.getCacheUrls(this);
            adapter = new CageProfileAdapter(this, photos);
            adapter.setOffline(true);
        }
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        rvPhotos.setLayoutManager(layoutManager);
        adapter.setLockedAnimations(animate);
        rvPhotos.setAdapter(adapter);
    }


    @Override
    public void calm() {
        url = UrlUtil.CALM;
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        reset(true);
    }

    @Override
    public void gray() {
        url = UrlUtil.GRAY;
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        reset(true);

    }

    @Override
    public void crazy() {
        url = UrlUtil.CRAAZY;
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        reset(true);

    }




    @Override
    public void cageProfile() {
        getDrawerLayout().closeDrawers();
    }

    @Override
    public void myProfile() {
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void setColumnCount(int count) {
        if(spanCount!=count) {
            adapter.setLockedAnimations(true);
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(count, StaggeredGridLayoutManager.VERTICAL);
            rvPhotos.setLayoutManager(layoutManager);
            spanCount = count;
        }
    }

    private void setupRevealBackground() {
        vRevealBackground.setOnStateChangeListener(this);
            final int[] startingLocation = new int[]{0,0};
            vRevealBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    vRevealBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                    vRevealBackground.startFromLocation(startingLocation);
                    return true;
                }
            });

    }

    @Override
    public void onStateChange(int state) {
        if (RevealBackgroundView.STATE_FINISHED == state) {
            rvPhotos.setVisibility(View.VISIBLE);
            reset(false);
        } else {
            rvPhotos.setVisibility(View.INVISIBLE);
        }
    }
}
