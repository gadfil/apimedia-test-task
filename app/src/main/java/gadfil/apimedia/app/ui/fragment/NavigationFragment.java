package gadfil.apimedia.app.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import gadfil.apimedia.app.R;

/**
 * Created by gadfil on 15.03.2015.
 */
public class NavigationFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener{

    ListView listView;
    FrameLayout vProfile;
    NavigateAction navigateAction;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);
        listView=(ListView)rootView.findViewById(R.id.vList);
        vProfile=(FrameLayout)rootView.findViewById(R.id.profile);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_drawer, R.id.text, getResources().getStringArray(R.array.menu_array));
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        vProfile.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                navigateAction.cageProfile();
                break;
            case 1:
                navigateAction.calm();
                break;
            case 2:
                navigateAction.gray();
                break;
            case 3:
                navigateAction.crazy();
                break;

        }
    }

    @Override
    public void onClick(View v) {
        navigateAction.myProfile();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            navigateAction = (NavigateAction) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigateAction.");
        }
    }
    public interface NavigateAction{
        public void calm();
        public void gray();
        public void crazy();
        public void cageProfile();
        public void myProfile();
    }
}
