package gadfil.apimedia.app.ui.activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import butterknife.InjectView;
import gadfil.apimedia.app.R;
import gadfil.apimedia.app.view.RevealBackgroundView;

public class MyProfileActivity extends BaseActivity implements RevealBackgroundView.OnStateChangeListener,
        View.OnClickListener {

    @InjectView(R.id.vRevealBackground)
    RevealBackgroundView vRevealBackground;
    @InjectView(R.id.vHeaderRoot)
    FrameLayout vHeder;
    @InjectView(R.id.bWrite)
    ImageButton bWrite;

    @InjectView(R.id.liner)
    LinearLayout linearLayout;

    @InjectView(R.id.email)
    View vEmail;

    @InjectView(R.id.mobilePhone)
    View vMobile;

    @InjectView(R.id.homePhone)
    View vHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getToolbar().setNavigationIcon(R.drawable.ic_navigation_arrow_back);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setupRevealBackground();
        bWrite.setOnClickListener(this);
        vEmail.setOnClickListener(this);
        vMobile.setOnClickListener(this);
        vHome.setOnClickListener(this);

    }

    private void setupRevealBackground() {
        vRevealBackground.setOnStateChangeListener(this);
        final int[] startingLocation = new int[]{0, 0};
        vRevealBackground.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                vRevealBackground.getViewTreeObserver().removeOnPreDrawListener(this);
                vRevealBackground.startFromLocation(startingLocation);
                return true;
            }
        });

    }


    @Override
    public void onStateChange(int state) {
        if (RevealBackgroundView.STATE_FINISHED == state) {
            vHeder.setVisibility(View.VISIBLE);
            bWrite.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
        } else {
            vHeder.setVisibility(View.INVISIBLE);
            bWrite.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected boolean shouldInstallDrawer() {
        return false;
    }

    private void write() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"gadfil12@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "A&P Media");
        startActivity(Intent.createChooser(intent, "A&P Media Send Email"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bWrite:
                write();
                break;
            case R.id.email:
                write();
                break;


        }
    }
}
