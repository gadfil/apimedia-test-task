package gadfil.apimedia.app.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import com.squareup.picasso.Target;
import gadfil.apimedia.app.R;
import gadfil.apimedia.app.model.Photo;
import gadfil.apimedia.app.util.CacheUtil;
import gadfil.apimedia.app.util.DebugLog;
import gadfil.apimedia.app.util.DimenUtil;
import gadfil.apimedia.app.util.UrlUtil;
import gadfil.apimedia.app.view.ResizableImageView;

import java.io.File;
import java.util.Arrays;

/**
 * Created by gadfil on 14.03.2015.
 */
public class SimplePhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    Photo[] photos;
    private boolean isOffline;


    public SimplePhotoAdapter(Context context, Photo[]photos) {
        this.context = context;
        this.photos = photos;
        isOffline = false;
        DebugLog.d("adapter", Arrays.toString(photos));
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        DebugLog.d("adapter", ""+position);
        DebugLog.d("adapter", ""+photos[position].getUrl());
        if(isOffline){
            Picasso.with(context)
                    .load(new File(photos[position].getUrl()))
                    .into(((PhotoViewHolder) holder).ivPhoto);
            DebugLog.d("url", new File(photos[position].getUrl()).getPath());
        }else {
            File file = new File(context.getApplicationContext().getCacheDir().getPath(),  CacheUtil.CACHE_DIR);
            file = new File(file, photos[position].getUrl());
            file = new File(file, CacheUtil.IMAGE_NAME);
            if(file.exists()){
                Picasso.with(context)
                        .load(file)
                        .into(((PhotoViewHolder) holder).ivPhoto);
            }else {
                Picasso.with(context)
                        .load(photos[position].getUrl())
                        .into(((PhotoViewHolder) holder).ivPhoto);
            }
        }

//Picasso.with(context)
//                .load(new File("/data/data/gadfil.apimedia.app/cache/http:/www.placecage.com/184/239/PlaceCage.jpg"))
//
//                .into(((PhotoViewHolder) holder).ivPhoto);


    }

    @Override
    public int getItemCount() {
        return photos.length;
    }

    public void setOffline(boolean offline){
        this.isOffline = offline;
    }

    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.ivPhoto)
        ResizableImageView ivPhoto;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
