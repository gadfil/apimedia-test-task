package gadfil.apimedia.app.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import butterknife.InjectView;
import butterknife.OnClick;
import gadfil.apimedia.app.R;
import gadfil.apimedia.app.model.Photo;
import gadfil.apimedia.app.ui.adapter.SimplePhotoAdapter;
import gadfil.apimedia.app.ui.fragment.NavigationFragment;
import gadfil.apimedia.app.util.CacheUtil;
import gadfil.apimedia.app.util.DebugLog;
import gadfil.apimedia.app.util.UrlUtil;

import java.util.Arrays;


public class MainActivity extends BaseActivity implements NavigationFragment.NavigateAction {

    @InjectView(R.id.rvPhotos)
    RecyclerView rvPhotos;
    @InjectView(R.id.bReset)
    Button bReset;

    Photo[] photos;
    SimplePhotoAdapter adapter;

    String url = UrlUtil.CALM;
    CacheUtil.CacheTask cacheTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvPhotos.setLayoutManager(layoutManager);
        cacheTask = new CacheUtil.CacheTask(this);
        reset();
        rvPhotos.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int[] firstVisibleItems = ((StaggeredGridLayoutManager) rvPhotos.getLayoutManager()).findFirstVisibleItemPositions(null);
                int[] lastComplete = ((StaggeredGridLayoutManager) rvPhotos.getLayoutManager()).findLastCompletelyVisibleItemPositions(null);
                int lastItem = 0;
                for (int i : lastComplete) {
                    lastItem = Math.max(lastItem, i);
                }
                int firstItem = photos.length;
                for (int i : firstVisibleItems) {
                    firstItem = Math.min(firstItem, i);
                }
                DebugLog.d("onScrolled", "last          " + lastItem);
                DebugLog.d("onScrolled", "first         " + firstItem);

                float newAlpha;
                if (firstItem == 0 || firstItem == -1) {
                    newAlpha = 1;
                } else {
                    newAlpha = 1 - 1 / (float) (photos.length - 1) * lastItem;
                }
                bReset.setAlpha(newAlpha);
            }
        });

        bReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

    }


    private void reset() {
        if (CacheUtil.isOnline(this)) {
            photos = new Photo[50];
            for (int i = 0; i < photos.length; i++) {
                photos[i] = UrlUtil.getPhoto(url, UrlUtil.MIN_SIZE, UrlUtil.MAX_SIZE);
            }
            if (cacheTask.getStatus() == AsyncTask.Status.RUNNING) {
                cacheTask.cancel(true);
            }
            cacheTask = new CacheUtil.CacheTask(this);
            cacheTask.execute(photos);
            adapter = new SimplePhotoAdapter(this, photos);
        } else {
            photos = CacheUtil.getCacheUrls(this);
            adapter = new SimplePhotoAdapter(this, photos);
            adapter.setOffline(true);
        }
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvPhotos.setLayoutManager(layoutManager);
        rvPhotos.setAdapter(adapter);
    }


    @Override
    public void calm() {
        url = UrlUtil.CALM;
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        reset();
    }

    @Override
    public void gray() {
        url = UrlUtil.GRAY;
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        reset();

    }

    @Override
    public void crazy() {
        url = UrlUtil.CRAAZY;
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        reset();

    }



    @Override
    public void cageProfile() {
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        Intent intent = new Intent(this, CageProfileActivity.class);
        startActivity(intent);

    }

    @Override
    public void myProfile() {
        cacheTask.cancel(true);
        getDrawerLayout().closeDrawers();
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
    }
}
