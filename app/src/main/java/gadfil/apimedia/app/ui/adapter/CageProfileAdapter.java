package gadfil.apimedia.app.ui.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.squareup.picasso.Picasso;
import gadfil.apimedia.app.R;
import gadfil.apimedia.app.model.Photo;
import gadfil.apimedia.app.util.CacheUtil;
import gadfil.apimedia.app.util.DebugLog;
import gadfil.apimedia.app.view.ResizableImageView;

import java.io.File;

/**
 * Created by gadfil on 15.03.2015.
 */
public class CageProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener{
    public static final int TYPE_PROFILE_HEADER = 0;
    public static final int TYPE_PROFILE_OPTIONS = 1;
    public static final int TYPE_PHOTO = 2;

    private static final int MIN_ITEMS_COUNT = 2;


    private static final int USER_OPTIONS_ANIMATION_DELAY = 300;
    private static final int MAX_PHOTO_ANIMATION_DELAY = 600;

    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();


    private Photo[]photos;
    private Context context;
    private boolean isOffline;
    OptionAction optionAction;
    private boolean lockedAnimations;
    private long profileHeaderAnimationStartTime = 0;
    private int lastAnimatedItem = 0;

    public CageProfileAdapter(Context context, Photo[] photos) {
        this.photos = photos;
        this.context = context;
        this.isOffline = false;

        try {
            optionAction = (OptionAction)context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement OptionAction.");
        }

    }
    public void setOffline(boolean offline){
        this.isOffline = offline;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (TYPE_PROFILE_HEADER == viewType) {
            final View view = LayoutInflater.from(context).inflate(R.layout.view_cage_profile_header, parent, false);
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            layoutParams.setFullSpan(true);
            view.setLayoutParams(layoutParams);
            return new ProfileHeaderViewHolder(view);
        } else if (TYPE_PROFILE_OPTIONS == viewType) {
            final View view = LayoutInflater.from(context).inflate(R.layout.view_options, parent, false);
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            layoutParams.setFullSpan(true);
            view.setLayoutParams(layoutParams);
            return new ProfileOptionsViewHolder(view);
        } else if (TYPE_PHOTO == viewType) {
            final View view = LayoutInflater.from(context).inflate(R.layout.item_photo, parent, false);
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            layoutParams.setFullSpan(false);
            view.setLayoutParams(layoutParams);
            return new PhotoViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (TYPE_PROFILE_HEADER == viewType) {
            bindProfileHeader((ProfileHeaderViewHolder) holder);
        } else if (TYPE_PROFILE_OPTIONS == viewType) {
            bindProfileOptions((ProfileOptionsViewHolder) holder);
        } else if (TYPE_PHOTO == viewType) {
            bindPhoto((PhotoViewHolder) holder, position);
        }
    }

    private void bindProfileOptions(final ProfileOptionsViewHolder holder) {

        holder.bGrid.setOnClickListener(this);
        holder.bFeed.setOnClickListener(this);
        holder.vRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                holder.vRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                animateUserProfileOptions(holder);
                return false;
            }
        });

    }




    private void bindProfileHeader(final ProfileHeaderViewHolder holder) {
        holder.root.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                holder.root.getViewTreeObserver().removeOnPreDrawListener(this);
                animateProfileHeader(holder);
                return false;
            }
        });
    }

    private void bindPhoto(final PhotoViewHolder holder, int position) {
        position-=MIN_ITEMS_COUNT;
        if(isOffline){
            Picasso.with(context)
                    .load(new File(photos[position].getUrl()))
                    .into( holder.ivPhoto);
            DebugLog.d("url", new File(photos[position].getUrl()).getPath());
        }else {
            File file = new File(context.getApplicationContext().getCacheDir().getPath(),  CacheUtil.CACHE_DIR);
            file = new File(file, photos[position].getUrl());
            file = new File(file, CacheUtil.IMAGE_NAME);
            if(file.exists()){
                Picasso.with(context)
                        .load(file)
                        .into(holder.ivPhoto);
            }else {
                Picasso.with(context)
                        .load(photos[position].getUrl())
                        .into(holder.ivPhoto);
            }
        }
    }

    @Override
    public int getItemCount() {
        return MIN_ITEMS_COUNT+photos.length;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return TYPE_PROFILE_HEADER;
        } else if (position == 1) {
            return TYPE_PROFILE_OPTIONS;
        } else {
            return TYPE_PHOTO;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bGrid:
                optionAction.setColumnCount(3);
                break;
            case R.id.bFeed:
                optionAction.setColumnCount(1);
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void animateProfileHeader(ProfileHeaderViewHolder viewHolder) {
        if (!lockedAnimations) {
            profileHeaderAnimationStartTime = System.currentTimeMillis();

            viewHolder.root.setTranslationY(-viewHolder.root.getHeight());
            viewHolder.ivProfilePhoto.setTranslationY(-viewHolder.ivProfilePhoto.getHeight());
            viewHolder.vNicName.setTranslationY(-viewHolder.vNicName.getHeight());
            viewHolder.vName.setTranslationY(-viewHolder.vName.getHeight());
            viewHolder.vLogin.setTranslationY(-viewHolder.vLogin.getHeight());

            viewHolder.root.animate().translationY(0).setDuration(300).setInterpolator(INTERPOLATOR);
            viewHolder.ivProfilePhoto.animate().translationY(0).setDuration(300).setStartDelay(100).setInterpolator(INTERPOLATOR);
            viewHolder.vNicName.animate().translationY(0).setDuration(300).setStartDelay(200).setInterpolator(INTERPOLATOR);
            viewHolder.vName.animate().translationY(0).setDuration(300).setStartDelay(200).setInterpolator(INTERPOLATOR);
            viewHolder.vLogin.animate().translationY(0).setDuration(300).setStartDelay(200).setInterpolator(INTERPOLATOR);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void animateUserProfileOptions(ProfileOptionsViewHolder viewHolder) {
        if (!lockedAnimations) {
            viewHolder.vRoot.setTranslationY(-viewHolder.vRoot.getHeight());
            viewHolder.vRoot.animate().translationY(0).setDuration(300).setStartDelay(USER_OPTIONS_ANIMATION_DELAY).setInterpolator(INTERPOLATOR);
        }
    }

    public void setLockedAnimations(boolean lockedAnimations) {
        this.lockedAnimations = lockedAnimations;
    }

    static class ProfileHeaderViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.ivProfile)
        ImageView ivProfilePhoto;
        @InjectView(R.id.vNicName)
        TextView vNicName;
        @InjectView(R.id.vName)
        TextView vName;
        @InjectView(R.id.vLogin)
        TextView vLogin;
        @InjectView(R.id.vRoot)
        View root;

        public ProfileHeaderViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    static class ProfileOptionsViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.bGrid)
        ImageButton bGrid;
        @InjectView(R.id.bFeed)
        ImageButton bFeed;
        @InjectView(R.id.vRoot)
        View vRoot;

        public ProfileOptionsViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.ivPhoto)
        ResizableImageView ivPhoto;
        @InjectView(R.id.vRoot)
        View vRoot;


        public PhotoViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
    public interface OptionAction{
        public void setColumnCount(int count);


    }
}
