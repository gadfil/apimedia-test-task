package gadfil.apimedia.app.ui.activity;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;

import android.view.Gravity;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import gadfil.apimedia.app.R;

/**
 * Created by gadfil on 14.03.2015.
 */
public abstract class BaseActivity extends ActionBarActivity {
    @Optional
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @Optional
    @InjectView(R.id.wDrawerLayout)
    DrawerLayout drawerLayout;
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.inject(this);
        setupToolbar();
        if(shouldInstallDrawer()){
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawerLayout.openDrawer(Gravity.START);
                }
            });
        }

    }

    protected void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_navigation_menu);
        }
    }

    protected boolean shouldInstallDrawer() {
        return true;
    }
    public Toolbar getToolbar() {
        return toolbar;
    }
    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }
}
